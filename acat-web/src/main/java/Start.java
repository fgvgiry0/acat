import org.apache.log4j.Logger;

import com.mango.acat.core.server.netty.NettyServer;
import com.mango.acat.core.setting.AppSetting;


public class Start{
	public Logger log = Logger.getLogger(getClass());
	
	

	public static void main(String[] args) throws Exception {
		/*if(args.length>0){
			System.out.println("参数："+args[0]);
			String arg  = args[0];
			if(arg!=null && arg.equals("productModel")){
				//产品模式
				Constant.contextPath = "";
			}
		}*/
		new NettyServer().start(AppSetting.app.getInt("app.port"));
	}
}