package com.mango.acat.core.util;

import com.mango.acat.core.setting.AppSetting;

public class Constant {
	public static final String contextPath;
	static{
		contextPath = AppSetting.app.getStr("app.content.path");
	}
}
